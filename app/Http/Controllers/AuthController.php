<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(Request $request) {
        return view('register');
    }

    public function welcome(Request $request){
        //dd ($request["last_name"]);
        $namaDepan = $request["first_name"];
        $namaBelakang = $request["last_name"];
        return view('welcome', compact("namaDepan", "namaBelakang"));
        
    }

}

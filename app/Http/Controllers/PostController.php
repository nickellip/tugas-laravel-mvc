<?php
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
 
use App\Post;
 
class PostController extends Controller
{
    public function create()
    {
    	return view('post.create');
    }
 
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'title' => 'required',
    		'body' => 'required'
    	]);
 
        Post::create([
    		'title' => $request->title,
    		'body' => $request->body
    	]);
 
    	return redirect('/posts');
    }

    public function index()
    {
        $post = Post::all();
        return view('post.index', compact('post'));
    }
    
    public function show($id)
    {
        $post = Post::find($id);
        return view('post.show', compact('post'));
    }
    
    public function edit($id)
    {
        $post = Post::find($id);
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required|unique:post',
            'body' => 'required',
        ]);

        $post = post::find($id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->update();
        return redirect('/posts');
    }
    
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect('/posts');
    }

}
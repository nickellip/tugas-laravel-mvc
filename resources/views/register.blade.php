<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>

    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="first name"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="last name"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="sex"> Male<br>
        <input type="radio" name="sex"> Female<br>
        <input type="radio" name="sex"> Other<br><br>
        <label>Nationality:</label> <br><br>
        <select name="nationality" > 
            <option value="indo">Indonesian</option><br>
            <option value="usa">American</option><br>
            <option value="eng">British</option><br>
            <option value="china">Chinese</option><br>
            <option value="tiles">Timorese</option><br>
            <option value="japan">Japanese</option><br>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="Bio" cols="33" rows="10"></textarea><br>
        <input type="submit" value="Sign Up"> <br>


    </form>
</body>
</html>
@extends('admin_lte.master')

@section('content')


<div class="card card-primary">

              <div class="card-header">
                <h3 class="card-title">Create New Cast</h3>
              </div><br>
              <!-- /.card-header -->
              <!-- form start -->
           
<div>

        <form role="form" action="/cast" method="POST">
            @csrf
            <div class="form-group">
            <div class="ml-3">
                <label for="nama" > Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama', '') }}" placeholder="Masukkan Nama" required>
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
            <div class="ml-3">
                <label for="umur">  Umur</label>
                <input type="number" class="form-control" name="umur" id="umur" value="{{ old('umur', '') }}" placeholder="Masukkan Umur" required>
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
            <div class="ml-3">
                <label for="bio">  Bio</label>
                <input type="text" class="form-control" name="bio" id="bio" value="{{ old('bio', '') }}" placeholder="Masukkan Bio" required>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div><br><br>
            <div class = "card-footer">
            <button type="submit" class="btn btn-primary">Tambahkan</button>
        </form>
</div>
@endsection


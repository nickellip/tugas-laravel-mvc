<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', 'HomeController@home');

//Route::get('/register', 'AuthController@register');

//Route::post('/welcome', 'AuthController@welcome');

//Tugas kedua

//Route::get('/table', function() {
   // return view('pages_al.table');
//});

//Route::get('/data-table', function() {
  //  return view('pages_al.dataTables');
//});

//Tugas keempat

//Route::get('/cast/create', 'CastController@create');
//Route::post('/cast', 'CastController@store');
//Route::get('/cast', 'CastController@index');
//Route::get('/cast/{cast_id}', 'CastController@show');
//Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//Route::put('/cast/{cast_id}', 'CastController@update');
//Route::delete('/cast/{cast_id}', 'CastController@destroy');


Route::resource('posts', 'PostController');
//Route::get('/post/create', 'PostController@create');
//Route::post('/post', 'PostController@store');
//Route::get('/post', 'PostController@index');
//Route::get('/post/{post_id}', 'PostController@show');
//Route::get('/post/{post_id}/edit', 'PostController@edit');
//Route::put('/post/{post_id}', 'PostController@update');
//Route::delete('/post/{post_id}', 'PostController@destroy');
//Route::get('/table', 'HomeController@home');

//Route::get('/data-tables', 'HomeController@home');

//Route::get('/', 'HomeController@home');